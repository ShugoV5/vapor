const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const K = require("./constants.js");
const SchemaID = K.CollectionID;




mongoose.connect("mongodb://localhost:27017/TestDB", {
  useNewUrlParser: true
});

const messageSchema = new Schema({
  Message: String,
  Reply: [{ type: Schema.ObjectId, ref: 'Replies' }]
});

const RepliesSchema =new Schema({
  content: String,
  ownerID: String,
})

const userSchema = new Schema({
  LastMessage: String,
  Name: String,
  UserID: String,
  EXP: String,
  Level: String
});

const User = mongoose.model("user", userSchema);
const Message = mongoose.model("message", messageSchema);
const Replies = mongoose.model("replies",RepliesSchema);

function isURL(aString) {
  var expression = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
  var regex = new RegExp(expression);
  return Boolean(aString.match(regex));
}

function find(data, ID,callback) {
    if(ID == SchemaID.Message) {
        Message.find({Message: data})
        .populate('content').populate('ownerID').exec(doc=>{
          console.log(doc)
          callback(doc)
        });
    } else if(ID==SchemaID.User){
        User.find({UserID: data},function(err,doc){
            callback(doc);
        });
    }
}

function deleteAll(data, ID) {
    if(ID == SchemaID.Message) {
        Message.find({Message: data}).remove().exec();
    } else {
        User.find({UserID: data}).remove().exec();
    }
}

function createForm(data, colID){
  if(colID == SchemaID.Replies) {
    return new Replies(data)
  } else if(colID == SchemaID.User){
    return new User(data);
  }
}
function saveMessage(message, replies) {
  replies.save(()=>{
    let msg = new Message({
      Message: message,
      Replies: replies._id,
    })
    msg.save();
  })
}

module.exports = {
  find,
  deleteAll,
  createForm,
  saveMessage,
}
  a = createForm({content:"Khoadzvailoz", ownerID:"9900022"},SchemaID.Replies)
  saveMessage("Khoadzvcl",a)

  find("Khoadzvcl",SchemaID.Message,doc=>{})