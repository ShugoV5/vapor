const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/Bot", {
  useNewUrlParser: true
});
const Message = mongoose.model("message", {
  // ID = 1
  message: String,
  reply: [String]
});
const Sticker = mongoose.model("sticker", {
  // ID = 2
  message: String,
  reply: [String]
});
const LastMSG = mongoose.model("lastmsg", {
  // ID = 3
  userID: String,
  lastmsg: String
});
const User = mongoose.model("user", {
  // ID = 4
  Name: String,
  userID: String,
  EXP: String,
  Level: String
});
const Thread = mongoose.model("thread", {
  // ID = 5
  ThreadID: String,
  ThreadName: String,
  MemberID: [String]
});
const Image = mongoose.model("image", {
  // ID = 6
  Message: String,
  SRC: [String]
});
const Game = mongoose.model("game", {
  // ID = 7
  UserID: String,
  Credit: String,
  TimeStamp: String
});

module.exports = {
  createForm: (ID, obj) => {
    // Create Form before saving
    if (ID === 1) {
      return new Message({
        message: obj.message,
        reply: obj.reply
      });
    } else if (ID === 2) {
      return new Sticker({
        message: obj.message,
        reply: obj.reply
      });
    } else if (ID === 3) {
      return new LastMSG({
        userID: obj.userID,
        lastmsg: obj.lastmsg
      });
    } else if (ID === 4) {
      return new User({
        Name: obj.Name,
        userID: obj.userID,
        EXP: obj.EXP,
        Level: obj.Level
      });
    } else if (ID === 5) {
      return new Thread({
        ThreadID: obj.ThreadID,
        ThreadName: obj.ThreadName,
        MemberID: obj.MemberID
      });
    } else if (ID === 6) {
      return new Image({
        Message: obj.Message,
        SRC: obj.SRC
      });
    } else if (ID === 7) {
      return new Game({
        UserID: obj.UserID,
        Credit: obj.Credit,
        TimeStamp: obj.TimeStamp
      });
    }
  },

  find: (data, ID, callback) => {
    if (ID === 1) {
      Message.find(
        {
          message: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 2) {
      Sticker.find(
        {
          message: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 3) {
      LastMSG.find(
        {
          userID: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 4) {
      User.find(
        {
          userID: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 5) {
      Thread.find(
        {
          ThreadID: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 6) {
      Image.find(
        {
          Message: data
        },
        (err, doc) => {
          callback(doc);
        }
      );
    } else if (ID === 7) {
      Game.find(
        { 
          UserID: data 
        }, 
        (err, doc) => {
        callback(doc);
      });
    }
  },

  delete: (data, ID) => {
    if (ID === 1) {
      Message.find(data)
        .remove()
        .exec();
    } else if (ID === 2) {
      Sticker.find(data)
        .remove()
        .exec();
    } else if (ID === 3) {
      LastMSG.find(data)
        .remove()
        .exec();
    } else if (ID === 4) {
      User.find(data)
        .remove()
        .exec();
    } else if (ID === 5) {
      Thread.find(data)
        .remove()
        .exec();
    } else if (ID === 6) {
      Image.find(data)
        .remove()
        .exec();
    }
  },
  updateMessage: (doc, reply) => {
    doc[0].reply = reply;
    doc[0].save();
  },
  updateLastMsg: (doc, lastmsg) => {
    doc[0].lastmsg = lastmsg;
    doc[0].save();
  },
  updateSticker: (doc, StickerID) => {
    doc[0].reply = StickerID;
    doc[0].save();
  },
  updateScore: (doc, exp, level) => {
    doc[0].EXP = exp;
    doc[0].Level = level;
    doc[0].save();
  },
  updateImg: (doc, SRC) => {
    doc[0].SRC = SRC;
    doc[0].save();
  },
  save: data => {
    data.save();
  }
};
