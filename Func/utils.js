const fs = require("fs");
const request = require("request");

module.exports = {
    dwlImg : (link, path, callback) => {
        request(link)
            .pipe(fs.createWriteStream(path))
            .on('close', ()=>{
                callback();
            });
    }
}