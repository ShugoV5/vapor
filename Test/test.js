const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/TestDB", {
  useNewUrlParser: true
});
const messageSchema = new Schema({
  Message: String,
  Reply: [
    {
      ID: { type: String, default: "Message" },
      Reply: [String]
    },
    {
      ID: { type: String, default: "Sticker" },
      StickerID: [String]
    },
    {
      ID: { type: String, default: "Image" },
      ImageSrc: [String]
    }
  ]
});

const userSchema = new Schema({
  LastMessage: String,
  Name: String,
  UserID: String,
  EXP: String,
  Level: String
});

const User = mongoose.model("user", userSchema);

const Message = mongoose.model("message", messageSchema);

function find(doc) {
    
}
