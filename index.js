const db = require("./Func/database_OLD.js");
const fs = require("fs");
const login = require("facebook-chat-api");
const random = require("./Func/random.js");
const ult = require("./Func/utils.js");
const MSG = 1;
const STICKER = 2;
const LASTMSG = 3;
const USER = 4;
const THREAD = 5;
const IMAGE = 6;

/*
    Lưu ý: msg là tinh nhắn, message là object, cẩn thận khi xài :>
*/

login(
    {
        appState: JSON.parse(fs.readFileSync("appstate.json", "utf8"))
    },
    (err, api) => {
        if (err) return console.error(err);

        api.setOptions({
            selfListen: false,
            logLevel: "silent"
        });

        api.listen((err, message) => {
            if (err) return console.error(err);
            var msg = message.body.toLowerCase();

            async function procCMI(comp, callback) {
                let c = {
                    mes: null,
                    stc: null
                };
                db.find(comp, MSG, data1 => {
                    if (data1 != "") {
                        c.mes = data1[0].reply;
                    }
                    db.find(comp, STICKER, data2 => {
                        if (data2 != "") {
                            c.stc = data2[0].reply;
                        }
                        callback(c);
                    });
                });
            }
            console.log(
                "_____________________\n" +
                message.threadID +
                "\n" +
                msg +
                "\n_____________________"
            ); //LOG
            if(msg.indexOf("tonether:")==0) {
                let x= msg.slice(9,msg.indexOf(" "));
                let Z= msg.slice(msg.indexOf(" "), message.length);
                let nX = parseInt(x)/8;
                let nZ = parseInt(Z)/8;
                api.sendMessage("Location: \nX: "+nX+"\nZ: "+nZ, message.threadID);
            }
            /*SET*/
            if (msg.indexOf("->") != -1) {
                let QA = msg.toLowerCase().split("->", 2);
                if (QA[0] != "" && QA[1] != "") {
                    db.find(QA[0].trim(), MSG, function (data) {
                        if (data != "") {
                            let oReply = data[0].reply;
                            if (oReply.indexOf(QA[1].trim()) != -1) {
                                api.sendMessage("Có sẵn r, set set cc", message.threadID);
                                api.setMessageReaction(":dislike:", message.messageID);
                            } else {
                                oReply.push(QA[1].trim());
                                api.setMessageReaction(":like:", message.messageID);
                                db.updateMessage(data, oReply);
                            }
                        } else {
                            db.save(
                                db.createForm(MSG, {
                                    message: QA[0].trim(),
                                    reply: QA[1].trim()
                                })
                            );
                        }
                    });
                }
            }
            // function dwlImg(link, path, callback) {
            //     request({uri:link})
            //         .pipe(fs.createWriteStream(path))
            //         .on("close", function() {
            //             callback();
            //         });
            // }

            if (msg.indexOf("set sticker ") == 0) {
                api.sendMessage("Gửi một nhãn dán... ", message.threadID);
            }

            if (msg.indexOf("#") == 0) {
                api.sendMessage("Gửi một ảnh... ", message.threadID);
            }

            /*ENDSET*/

            /*DELETE*/
            if (msg.indexOf("del ") == 0) {
                let dot = msg.slice(4, msg.lenth);
                console.log(dot);
                db.delete(
                    {
                        message: dot
                    },
                    MSG
                );
                db.delete(
                    {
                        message: dot
                    },
                    STICKER
                );
                db.delete({Message: dot},IMAGE);
                api.setMessageReaction(":like:", message.messageID);
            }

            /*CMI*/
            if (msg.indexOf(".cmi ") == 0) {
                let comp = msg.slice(5, msg.length);
                procCMI(comp, data => {
                    console.log(data);
                    api.sendMessage(
                        "Message: " +
                        comp +
                        "\nReply: " +
                        data.mes +
                        "\nSticker: " +
                        data.stc,
                        message.threadID
                    );
                });
                api.setMessageReaction(":like:", message.messageID);
            }

            /*CHECK*/

            if (message.type == "message") {
                //check message
                db.find(msg, MSG, data => {
                    if (data != "") {
                        let reply = random.ArrRand(data[0].reply);
                        api.sendMessage(reply, message.threadID);
                    }
                });

                //check sticker
                db.find(msg, STICKER, data => {
                    if (data != "") {
                        let ID = random.ArrRand(data[0].reply);
                        api.sendMessage(
                            {
                                sticker: ID
                            },
                            message.threadID
                        );
                    }
                });
                //check image
                db.find(msg, IMAGE, data=> {
                    if(data!= "") {
                        let ImgSrc = random.ArrRand(data[0].SRC);
                        ult.dwlImg(ImgSrc,"./src/"+message.senderID+".png",()=>{
                            let m = {body:"", attachment: fs.createReadStream("./src/"+message.senderID+".png")}
                            api.sendMessage(m, message.threadID,(err, msginfo)=>{
                                fs.unlink("./src/"+message.senderID+".png",(err)=>{});
                            })
                        });
                    }
                })

                //save last message
                db.find(message.senderID, LASTMSG, data => {
                    if (data != "") {
                        db.updateLastMsg(data, msg);
                    } else {
                        db.save(
                            db.createForm(LASTMSG, {
                                userID: message.senderID,
                                lastmsg: msg
                            })
                        );
                    }
                });
                //cal rank
                db.find(message.senderID, USER, data => {
                    if (data == "") {
                        api.getUserInfo(message.senderID, (err, ret) => {
                            db.save(
                                db.createForm(USER, {
                                    Name: ret[message.senderID].name,
                                    userID: message.senderID,
                                    EXP: 1,
                                    Level: 1
                                })
                            );
                        });
                    } else {
                        let currentScore = parseInt(data[0].EXP);
                        let currentLv = parseInt(data[0].Level);
                        let newScore = currentScore + random.IntRand(30);
                        let newLvStreak = Math.floor(newScore / (150 * currentLv));
                        let newLv = currentLv + newLvStreak;
                        db.updateScore(data, newScore, newLv);
                    }
                });
            }

            if (message.type == "message" && message.attachments != "") {
                if (message.attachments[0].type == "sticker") {
                    db.find(message.senderID, LASTMSG, data => {
                        if (data != "" && data[0].lastmsg.indexOf("set sticker ") == 0) {
                            let StickerID = message.attachments[0].ID;
                            let msgd = data[0].lastmsg
                                .toLowerCase()
                                .slice(12, data[0].length);
                            db.find(msgd, STICKER, doc => {
                                if (doc != "") {
                                    if (doc[0].reply.indexOf(StickerID) != -1) {
                                        api.sendMessage("Đã có trong hệ thống!", message.threadID);
                                    } else {
                                        api.setMessageReaction(":like:", message.messageID);
                                        let oReply = doc[0].reply;
                                        oReply.push(StickerID);
                                        db.updateSticker(doc, oReply);
                                        console.log(
                                            "---\nUPDATE STICKER\n" +
                                            doc +
                                            "\nID: " +
                                            oReply +
                                            "\n---"
                                        );
                                    }
                                } else {
                                    api.setMessageReaction(":like:", message.messageID);
                                    db.save(
                                        db.createForm(STICKER, {
                                            message: msgd,
                                            reply: StickerID
                                        })
                                    );
                                }
                            });
                        }
                    });
                }
            }

            if (message.type == "message" && message.attachments != "") {
                if (message.attachments[0].type == "photo") {
                    db.find(message.senderID, LASTMSG, data => {
                        if (data != "" && data[0].lastmsg.indexOf("#") == 0) {
                            let ImgSrc = message.attachments[0].url;
                            let msgd = data[0].lastmsg.slice(1, data[0].length);
                            db.find(msgd, IMAGE, doc => {
                                if (doc != "") {
                                    if (doc[0].SRC.indexOf(ImgSrc) != -1) {
                                        api.sendMessage(
                                            "Đã có trong hệ thống... ",
                                            message.threadID
                                        );
                                    } else {
                                        api.setMessageReaction(":like:", message.messageID);
                                        let OSrc = doc[0].SRC;
                                        OSrc.push(ImgSrc);
                                        db.updateImg(doc, OSrc);
                                    }
                                } else {
                                    api.setMessageReaction(":like:", message.messageID);
                                    db.save(db.createForm(IMAGE, { Message: msgd, SRC: ImgSrc }));
                                }
                            });
                        }
                    });
                }
            }

            /*SAY*/
            if (msg.indexOf("say ") == 0) {
                let say = require("./Func/Say.js");
                let lang = msg.slice(4, 6);
                let text = msg.slice(7, msg.length);
                if (lang == "en") {
                    say(text, "en", function () {
                        let m = {
                            body: "",
                            attachment: fs.createReadStream(__dirname + "/Func/src/say.mp3")
                        };
                        api.sendMessage(m, message.threadID);
                    });
                } else if (lang == "ja") {
                    say(text, "ja", function () {
                        let m = {
                            body: "",
                            attachment: fs.createReadStream(__dirname + "/Func/src/say.mp3")
                        };
                        api.sendMessage(m, message.threadID);
                    });
                } else {
                    let diftext = msg.slice(4, msg.length);
                    say(diftext, "vi", function () {
                        let m = {
                            body: "",
                            attachment: fs.createReadStream(__dirname + "/Func/src/say.mp3")
                        };
                        api.sendMessage(m, message.threadID);
                    });
                }
            }

            if (msg == ".rank") {
                db.find(message.senderID, USER, data => {
                    if (data != "") {
                        api.sendMessage(
                            "EXP: " + data[0].EXP + "\nLevel: " + data[0].Level,
                            message.threadID
                        );
                        console.log(
                            "---\nRanked of ID " +
                            message.senderID +
                            "\nEXP: " +
                            data[0].EXP +
                            "\nLevel: " +
                            data[0].Level +
                            "\n---"
                        );
                    }
                });
            }
        });
    }
);
