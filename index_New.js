const db = require("./Func/db.js");
const fs = require("fs");
const login = require("facebook-chat-api");
const random = require("./Func/random.js");
const ult = require("./Func/utils.js");
const K = require('./Func/constants');
const SchemaID = K.CollectionID;

/*
    Lưu ý: msg là tin nhắn, message là object, cẩn thận khi xài :>
*/

login(
  {
    appState: JSON.parse(fs.readFileSync("appstate.json", "utf8"))
  },
  (err, api) => {
    if (err) return console.error(err);

    api.setOptions({
      selfListen: false,
      logLevel: "silent"
    });

    api.listen((err, message) => {
      if (err) return console.error(err);
      var msg = message.body.toLowerCase();

      console.log(
        "_____________________\n" +
          message.threadID +
          "\n" +
          msg +
          "\n_____________________"
      ); //LOG
      if (msg.indexOf("tonether:") == 0) {
        let x = msg.slice(9, msg.indexOf(" "));
        let Z = msg.slice(msg.indexOf(" "), message.length);
        let nX = parseInt(x) / 8;
        let nZ = parseInt(Z) / 8;
        api.sendMessage(
          "Location: \nX: " + nX + "\nZ: " + nZ,
          message.threadID
        );
      }
      /*SET*/

      if (msg.indexOf("set sticker ") == 0) {
        api.sendMessage("Gửi một nhãn dán... ", message.threadID);
      }

      if (msg.indexOf("#") == 0) {
        api.sendMessage("Gửi một ảnh... ", message.threadID);
      }

      if(msg.indexOf("->")!=-1) {
        db.find(msg,SchemaID.Message, doc=>{
          console.log(doc);
        })
      }

      /*ENDSET*/

      /*SAY*/
      if (msg.indexOf("say ") == 0) {
        let say = require("./Func/Say.js");
        let text = msg.slice(6, msg.length);
        let lang = msg.slice(4, 6);
        if (lang.trim() != "en" && lang.trim() != "ja") {
          lang = "vi";
          text = msg.slice(4, msg.length);
        }
        say(text, lang, function() {
          let m = {
            body: "",
            attachment: fs.createReadStream(__dirname + "/Func/src/say.mp3")
          };
          api.sendMessage(m, message.threadID);
        });
      }


    });
  }
);
